/** FROM https://www.kevinleary.net/javascript-get-url-parameters/
 * JavaScript Get URL Parameter
 * 
 * @param String prop The specific URL parameter you want to retreive the value for
 * @return String|Object If prop is provided a string value is returned, otherwise an object of all properties is returned
 */
function getUrlParams( prop ) {
    var params = {};
    var search = decodeURIComponent( window.location.href.slice( window.location.href.indexOf( '?' ) + 1 ) );
    var definitions = search.split( '&' );

    definitions.forEach( function( val, key ) {
        var parts = val.split( '=', 2 );
        params[ parts[ 0 ] ] = parts[ 1 ];
    } );

    return ( prop && prop in params ) ? params[ prop ] : params;
}

function animateCSS(element, animationName, callback) {
    const node = document.querySelector(element)
    node.classList.add('animated', animationName)

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName)
        node.removeEventListener('animationend', handleAnimationEnd)

        if (typeof callback === 'function') callback()
    }

    node.addEventListener('animationend', handleAnimationEnd)
}
var score = 0;
// A $( document ).ready() block.
$( document ).ready(function() {
    var num_challenges=0;
    var active_challenge=0;
    var answers=[];
    var rectangles=[];
    var allow_help = true;

    $.getJSON('activities/' + getUrlParams()['id'] + '.json', function(data) {
        $(document).prop('title', data['title']);
        $("#map_img").attr("src", data['map_image_url']);
        $("#question_text").html("Βρες τα σημεία στην εικόνα:<br /><span id='active_point'><span>");


        if (typeof data['allow_help'] !== 'undefined') {
            if (data['allow_help']=='false')
                allow_help = false;
        }
        if (!allow_help)
            $("#hint_button").hide();

        $('#map_img').click(function(e) {
            var offset = $(this).offset();
            var X = (e.pageX - offset.left);
            var Y = (e.pageY - offset.top);
            //console.log('X: ' + X + ', Y: ' + Y);
            if (is_click_in_rect(X, Y, rectangles)==(active_challenge)) {


                $("#feedback_img").html("<img src='img/right.png'>");
                animateCSS('#question_text', 'flash');
                $("#feedback_img").css("margin-left", $("#map_img").width()/2-100);
                $("#feedback_img").css("margin-top", $("#map_img").height()/2-200);
                $("#feedback_img").show();
                animateCSS('#feedback_img', 'flash');
                setTimeout(function(){ 
                    $("#feedback_img").hide();
                }, 1000);
                
                animateCSS('#question_text', 'bounceOutRight', function() {
                    active_challenge++;
                    if (active_challenge<num_challenges)
                        $("#active_point").html(answers[active_challenge]);
                    else {
                        $("#question_text").html("");
                        if (score==0)
                            $("#score").html("Μπράβο! Το βρήκες με την πρώτη!");
                        else if (score==1)
                            $("#score").html("Χρειάστηκες " + score + " κλικ. Είμαι σίγουρος ότι μπορείς και καλύτερα...");
                        else 
                            $("#score").html("Χρειάστηκες " + score + " κλικς. Είμαι σίγουρος ότι μπορείς και καλύτερα...");
                        if (score<5) 
                            $("#map_img").attr("src", "img/happy_winner.gif");
                        else
                            $("#map_img").attr("src", "img/patrick-star-crying.gif");
                    }
                  })
            }
            else {
                $("#feedback_img").html("<img src='img/wrong.png'>");
                animateCSS('#question_text', 'flash');
                $("#feedback_img").css("margin-left", $("#map_img").width()/2-100);
                $("#feedback_img").css("margin-top", $("#map_img").height()/2-200);
                $("#feedback_img").show();
                animateCSS('#feedback_img', 'flash');
                setTimeout(function(){ 
                    $("#feedback_img").hide();
                }, 1000);
                score+=1;
                console.log(score);
            }
        });

        jQuery.each(data['points'], function(i, val) {
            answers.push(val['title']);
            rectangles.push(val['coords']);
            num_challenges++;
            var coords=val['coords'].split(",");
            hint_x = ( parseInt(coords[0]) + parseInt(coords[2]) ) / 2 - 10;
            hint_y = ( parseInt(coords[1]) + parseInt(coords[3]) ) / 2 - 10;
            child = "<img src='img/annotation.png' class='alx_hint' style='position:absolute; top:" + hint_y + "px; left:" + hint_x + "px; z-index:2;' />";
            //console.log(child);
            $('#mainitem').append(child);
        });

        $("#active_point").html(answers[active_challenge]);
       
        $("#hint_button").click(function(e) {
            if ($("#hint_button").text()=='Εμφάνιση βοήθειας') {
                $("#hint_button").text("Απόκρυψη βοήθειας");
                $(".alx_hint").css("display", "block");
            } else {
                $("#hint_button").text("Εμφάνιση βοήθειας");
                $(".alx_hint").css("display", "none");
            }
        });

        function is_click_in_rect(mouse_x, mouse_y, defined_rectangles) {
            for (w=0; w<defined_rectangles.length;w++) {
                var coords=defined_rectangles[w].split(",");
                x_low = parseInt(coords[0]); x_high = parseInt(coords[2]);
                y_low = parseInt(coords[1]); y_high = parseInt(coords[3]);
                if (x_low>x_high) 
                    x_high = [x_low, x_low = x_high][0];
                if (y_low>y_high) 
                    y_high = [y_low, y_low = y_high][0];
                
                if (mouse_x>x_low && mouse_x<x_high && mouse_y>y_low && mouse_y<y_high)
                    return w;
            }
            return -1;
        }
    })
    .fail(function() {
        console.log( "Unable to load activity file" );
        $("#hint_button").hide();
        $(document).prop('title', 'Διαθέσιμες δραστηριότητες');
        $("#map_img").attr("src", '');

        $.get('quizes_list',function(txt){
            var lines = txt.split("\n");
            activity_id='';
            for (var i = 0, len = lines.length; i < len; i++) {
                if (i%2==0)
                    activity_id=lines[i];
                if (i%2==1) {
                    url = "<a href='?id=" + activity_id + "'>" + lines[i].substring(1, lines[i].length-1) + "</a><br />";
                    $('#mainitem').append(url);
                }
                
            }
        }); 


        
    });

    
});